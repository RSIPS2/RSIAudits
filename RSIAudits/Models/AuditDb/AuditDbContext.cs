﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RSIAudits.Models.AuditDb
{
    public class AuditDbContext : DbContext
    {
        public AuditDbContext() : base("AuditDbConnectionString")
        {

        }

        public DbSet<GeneralLog> GeneralLogs { get; set; }
        public DbSet<ProductSaleLog> ProductSaleLogs { get; set; }
        public DbSet<ProductWatchLog> ProductWatchLogs { get; set; }
        public DbSet<UserSubscriptionLog> UserSubscriptionLogs { get; set; }
    }
}