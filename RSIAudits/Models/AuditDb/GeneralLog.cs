﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RSICommons.CommonModels;
using RSICommons.CommonModels.DTO.Enums;

namespace RSIAudits.Models.AuditDb
{
    public class GeneralLog
    {
        public long Id { get; set; }
        public DateTime LogTime { get; set; }
        public string LogMessage { get; set; }
        public MethodType MethodTypeEnum { get; set; }
    }
}