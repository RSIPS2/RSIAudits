﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSIAudits.Models.AuditDb
{
    public class ProductWatchLog
    {
        public long Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
        public DateTime WatchTime { get; set; }
    }
}