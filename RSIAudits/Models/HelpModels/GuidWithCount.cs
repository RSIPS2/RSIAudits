﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSIAudits.Models.HelpModels
{
    public class GuidWithCount
    {
        public Guid Guid { get; set; }
        public int Count { get; set; }

        public GuidWithCount()
        {
        }

        public GuidWithCount(Guid guid, int count)
        {
            this.Guid = guid;
            this.Count = count;
        }
    }
}