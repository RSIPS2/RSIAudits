﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace RSIAudits
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "GetTest",
                routeTemplate: "api/Audit/GetTest/{i}",
                defaults: new { controller = "Audit", action = "GetTest" }
            );

            config.Routes.MapHttpRoute(
                name: "postLogProductSale",
                routeTemplate: "api/Audit/postLogProductSale/{productId}/{userId}/{saleTime}",
                defaults: new { controller = "Audit", action = "postLogProductSale" }
                );

            config.Routes.MapHttpRoute(
                name: "getSoldProductCountByTimeRange",
                routeTemplate: "api/Audit/getSoldProductCountByTimeRange/{productId}/{dateFrom}/{dateTo}",
                defaults: new { controller = "Audit", action = "getSoldProductCountByTimeRange" }
                );

            config.Routes.MapHttpRoute(
                name: "getSoldProductMostPopular",
                routeTemplate: "api/Audit/getSoldProductMostPopular/{count}",
                defaults: new { controller = "Audit", action = "getSoldProductMostPopular" }
                );

            config.Routes.MapHttpRoute(
                name: "postLogProductWatch",
                routeTemplate: "api/Audit/postLogProductWatch/{productId}/{userId}/{watchTime}",
                defaults: new { controller = "Audit", action = "postLogProductWatch" }
                );

            config.Routes.MapHttpRoute(
                name: "getWatchedProductCountByTimeRange",
                routeTemplate: "api/Audit/getWatchedProductCountByTimeRange/{productId}/{dateFrom}/{dateTo}",
                defaults: new { controller = "Audit", action = "getWatchedProductCountByTimeRange" }
                );

            config.Routes.MapHttpRoute(
                name: "getWatchedProductMostPopular",
                routeTemplate: "api/Audit/getWatchedProductMostPopular/{count}",
                defaults: new { controller = "Audit", action = "getWatchedProductMostPopular" }
                );

            config.Routes.MapHttpRoute(
                name: "postLogUserSubscription",
                routeTemplate: "api/Audit/postLogUserSubscription/{productId}/{userId}/{subsctiptionTime}",
                defaults: new { controller = "Audit", action = "postLogUserSubscription" }
                );

            config.Routes.MapHttpRoute(
                name: "getUserSubscription",
                routeTemplate: "api/Audit/getUserSubscription/{userId}",
                defaults: new { controller = "Audit", action = "getUserSubscription" }
                );

            config.Routes.MapHttpRoute(
                name: "postLogGeneralLog",
                routeTemplate: "api/Audit/postLogGeneralLog/{methodTypeEnum}/{logMessage}",
                defaults: new { controller = "Audit", action = "postLogGeneralLog" }
                );

            config.Routes.MapHttpRoute(
                name: "getGeneralLogs",
                routeTemplate: "api/Audit/getGeneralLogs/{methodTypeEnum}/{dateFrom}/{dateTo}",
                defaults: new { controller = "Audit", action = "getGeneralLogs" }
                );

            config.Routes.MapHttpRoute(
                name: "getLastGeneralLogs",
                routeTemplate: "api/Audit/getLastGeneralLogs/{methodTypeEnum}/{count}",
                defaults: new { controller = "Audit", action = "getLastGeneralLogs" }
                );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultAuditApi",
                routeTemplate: "api/{Controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}
