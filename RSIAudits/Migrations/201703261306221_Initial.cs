namespace RSIAudits.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GeneralLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LogTime = c.DateTime(nullable: false),
                        LogMessage = c.String(),
                        MethodTypeEnum = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductSaleLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        SaleTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductWatchLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        WatchTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserSubscriptionLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        SubsctiptionTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserSubscriptionLogs");
            DropTable("dbo.ProductWatchLogs");
            DropTable("dbo.ProductSaleLogs");
            DropTable("dbo.GeneralLogs");
        }
    }
}
