﻿using RSIAudits.Models.AuditDb;
using RSIAudits.Models.HelpModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSIAudits.Repositories
{
    public class ProductWatchLogRepository
    {
        public bool AddProductWatch(ProductWatchLog log)
        {
            try
            {
                using (var db = new AuditDbContext())
                {
                    db.ProductWatchLogs.Add(log);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                return false;
            }
        }

        public List<ProductWatchLog> GetAll()
        {
            using (var db = new AuditDbContext())
            {
                List<ProductWatchLog> list = db.ProductWatchLogs.ToList();
                return list;
            }
        }

        public ProductWatchLog getById(long id)
        {
            using (var db = new AuditDbContext())
            {
                List<ProductWatchLog> list = db.ProductWatchLogs.Where(x => x.Id == id).ToList();
                if (list.Count > 0)
                {
                    return list.FirstOrDefault();
                }
                else
                {
                    return null;
                }

            }
        }

        public List<ProductWatchLog> GetAllByProductIdAndTimeRange(Guid productId, DateTime dateFrom, DateTime dateTo)
        {
            using (var db = new AuditDbContext())
            {
                List<ProductWatchLog> list = db.ProductWatchLogs.
                    Where(x => x.ProductId == productId && x.WatchTime > dateFrom && x.WatchTime < dateTo).ToList();
                return list;
            }
        }

        public List<GuidWithCount> GetMostPopularProductWatch(int topCount)
        {
            using (var db = new AuditDbContext())
            {
                var list = db.ProductWatchLogs.
                    GroupBy(x => x.ProductId).Select(g => new { ProductID = g.Key, Count = g.Count() }).OrderByDescending(grouped => grouped.Count).
                    Select(a => new GuidWithCount() { Guid = a.ProductID, Count = a.Count }).Take(topCount).ToList();
                return list;
            }
        }

    }
}