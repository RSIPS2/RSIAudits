﻿using RSIAudits.Models.AuditDb;
using RSIAudits.Models.HelpModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSIAudits.Repositories
{
    public class ProductSaleLogRepository
    {
        public bool AddProductSale(ProductSaleLog log)
        {
            try
            {
                using (var db = new AuditDbContext())
                {
                    db.ProductSaleLogs.Add(log);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                return false;
            }
        }

        public List<ProductSaleLog> GetAll()
        {
            using (var db = new AuditDbContext())
            {
                List<ProductSaleLog> list = db.ProductSaleLogs.ToList();
                return list;
            }
        }

        public ProductSaleLog getById(long id)
        {
            using (var db = new AuditDbContext())
            {
                List<ProductSaleLog> list = db.ProductSaleLogs.Where(x => x.Id == id).ToList();
                if (list.Count > 0)
                {
                    return list.FirstOrDefault();
                }
                else
                {
                    return null;
                }

            }
        }

        public List<ProductSaleLog> GetAllByProductIdAndTimeRange(Guid productId, DateTime dateFrom, DateTime dateTo)
        {
            using (var db = new AuditDbContext())
            {
                List<ProductSaleLog> list = db.ProductSaleLogs.
                    Where(x => x.ProductId == productId && x.SaleTime > dateFrom && x.SaleTime < dateTo).ToList();
                return list;
            }
        }

        public List<GuidWithCount> GetMostPopularProductSale(int topCount)
        { 
            using (var db = new AuditDbContext())
            {
                var list = db.ProductSaleLogs.
                    GroupBy(x => x.ProductId).Select(g => new { ProductID = g.Key, Count = g.Count() }).OrderByDescending(grouped => grouped.Count).
                    Select(a => new GuidWithCount() { Guid = a.ProductID, Count = a.Count}).Take(topCount).ToList();
                return list;
            }
        }

    }
}