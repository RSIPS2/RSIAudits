﻿using RSIAudits.Models.AuditDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSIAudits.Repositories
{
    public class UserSubscriptionLogRepository
    {
        public bool AddUserSubscription(UserSubscriptionLog log)
        {
            try
            {
                using (var db = new AuditDbContext())
                {
                    db.UserSubscriptionLogs.Add(log);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                return false;
            }
        }

        public List<UserSubscriptionLog> GetAll()
        {
            using (var db = new AuditDbContext())
            {
                List<UserSubscriptionLog> list = db.UserSubscriptionLogs.ToList();
                return list;
            }
        }

        public UserSubscriptionLog getById(long id)
        {
            using (var db = new AuditDbContext())
            {
                List<UserSubscriptionLog> list = db.UserSubscriptionLogs.Where(x => x.Id == id).ToList();
                if (list.Count > 0)
                {
                    return list.FirstOrDefault();
                }
                else
                {
                    return null;
                }

            }
        }

        public List<UserSubscriptionLog> GetAllByUserIdAndTimeRange(Guid userId, DateTime dateFrom, DateTime dateTo)
        {
            using (var db = new AuditDbContext())
            {
                List<UserSubscriptionLog> list = db.UserSubscriptionLogs.
                    Where(x => x.UserId == userId && x.SubsctiptionTime > dateFrom && x.SubsctiptionTime < dateTo).ToList();
                return list;
            }
        }

    }
}