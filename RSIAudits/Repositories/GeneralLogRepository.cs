﻿using RSIAudits.Models.AuditDb;
using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSIAudits.Repositories
{
    public class GeneralLogRepository
    {
        public bool AddGeneralLogl(GeneralLog log)
        {
            try
            {
                using (var db = new AuditDbContext())
                {
                    db.GeneralLogs.Add(log);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                return false;
            }
        }

        public List<GeneralLog> GetAllByMethodTypeEnumAndTimeRange(MethodType methodTypeEnum, DateTime dateFrom, DateTime dateTo)
        {
            using (var db = new AuditDbContext())
            {
                List<GeneralLog> list = db.GeneralLogs.
                    Where(x => x.MethodTypeEnum == methodTypeEnum && x.LogTime > dateFrom && x.LogTime < dateTo).ToList();
                return list;
            }
        }

        public List<GeneralLog> GetTopByMethodTypeEnum(MethodType methodTypeEnum, int topCount)
        {
            using (var db = new AuditDbContext())
            {
                List<GeneralLog> list = db.GeneralLogs.
                    Where(x => x.MethodTypeEnum == methodTypeEnum).OrderByDescending(y => y.LogTime).Take(topCount).ToList();
                return list;
            }
        }

    }
}