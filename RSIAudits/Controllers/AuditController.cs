﻿using RSIAudits.App_Start;
using RSIAudits.Models.AuditDb;
using RSIAudits.Models.HelpModels;
using RSIAudits.Repositories;
using RSICommons.CommonModels;
using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RSIAudits.Controllers
{
    [RequireHttps]

    public class AuditController : ApiController
    {
        public HttpResponseMessage GetTest(int i)
        {
            var sth = new List<string> { "test", "test2" };

            ProductSaleLog productSaleLog = new ProductSaleLog();
            productSaleLog.ProductId = new Guid();
            productSaleLog.SaleTime = DateTime.UtcNow;
            productSaleLog.UserId = new Guid();

            ProductSaleLogRepository repo = new ProductSaleLogRepository();
            repo.AddProductSale(productSaleLog);
            List<ProductSaleLog> logs = repo.GetAll();

            return Request.CreateResponse(HttpStatusCode.OK, new ApiResult { status = true, result = sth });
        }

        #region Sale

        [HttpGet]
        public HttpResponseMessage postLogProductSale(Guid productId, Guid userId, DateTime saleTime)
        {
            ProductSaleLog productSaleLog = new ProductSaleLog();
            productSaleLog.ProductId = productId;
            productSaleLog.SaleTime = saleTime;
            productSaleLog.UserId = userId;
            ProductSaleLogRepository saleLogRepository = new ProductSaleLogRepository();
            if (saleLogRepository.AddProductSale(productSaleLog))
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public HttpResponseMessage getSoldProductCountByTimeRange(Guid productId, DateTime dateFrom, DateTime dateTo)
        {
            List<ProductSaleLog> logsList = new ProductSaleLogRepository().GetAllByProductIdAndTimeRange(productId, dateFrom, dateTo);
            if (logsList != null && logsList.Count >= 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Count = logsList.Count });
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public HttpResponseMessage getSoldProductMostPopular(int count)
        {
            if (count > 0)
            {
                List<GuidWithCount> products = new ProductSaleLogRepository().GetMostPopularProductSale(count);
                if (products != null && products.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Products = products });
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

        }

        #endregion

        #region Watch

        [HttpGet]
        public HttpResponseMessage postLogProductWatch(Guid productId, Guid userId, DateTime watchTime)
        {
            ProductWatchLog productWatchLog = new ProductWatchLog();
            productWatchLog.ProductId = productId;
            productWatchLog.WatchTime = watchTime;
            productWatchLog.UserId = userId;
            ProductWatchLogRepository watchLogRepository = new ProductWatchLogRepository();
            if (watchLogRepository.AddProductWatch(productWatchLog))
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public HttpResponseMessage getWatchedProductCountByTimeRange(Guid productId, DateTime dateFrom, DateTime dateTo)
        {
            List<ProductWatchLog> logsList = new ProductWatchLogRepository().GetAllByProductIdAndTimeRange(productId, dateFrom, dateTo);
            if (logsList != null && logsList.Count >= 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Count = logsList.Count });
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public HttpResponseMessage getWatchedProductMostPopular(int count)
        {
            if (count > 0)
            {
                List<GuidWithCount> products = new ProductWatchLogRepository().GetMostPopularProductWatch(count);
                if (products != null && products.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Products = products });
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

        }

        #endregion

        #region Subsctiption

        [HttpGet]
        public HttpResponseMessage postLogUserSubscription(Guid productId, Guid userId, DateTime subsctiptionTime)
        {
            UserSubscriptionLog userSubscriptionLog = new UserSubscriptionLog();
            userSubscriptionLog.ProductId = productId;
            userSubscriptionLog.SubsctiptionTime = subsctiptionTime;
            userSubscriptionLog.UserId = userId;
            UserSubscriptionLogRepository subscriptionLogRepositor = new UserSubscriptionLogRepository();
            if (subscriptionLogRepositor.AddUserSubscription(userSubscriptionLog))
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public HttpResponseMessage getUserSubscription(Guid userId)
        {
            List<UserSubscriptionLog> logsList = new UserSubscriptionLogRepository().GetAllByUserIdAndTimeRange(userId, new DateTime().AddYears(-50), new DateTime());
            if (logsList != null && logsList.Count >= 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { UserSubscriptions = logsList.Count });
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        #endregion

        #region GeneralLog

        [HttpGet]
        public HttpResponseMessage postLogGeneralLog(MethodType methodTypeEnum, string logMessage)
        {
            GeneralLog generalLog = new GeneralLog();
            generalLog.LogMessage = logMessage;
            generalLog.MethodTypeEnum = methodTypeEnum;
            generalLog.LogTime = DateTime.UtcNow;

            GeneralLogRepository generalLogRepository = new GeneralLogRepository();
            if (generalLogRepository.AddGeneralLogl(generalLog))
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public HttpResponseMessage getGeneralLogs(MethodType methodTypeEnum, DateTime dateFrom, DateTime dateTo)
        {
            List<GeneralLog> logsList = new GeneralLogRepository().GetAllByMethodTypeEnumAndTimeRange(methodTypeEnum, dateFrom, dateTo);
            if (logsList != null && logsList.Count >= 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Logs = logsList.Select(y => new { y.LogTime, y.LogMessage }) });
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public HttpResponseMessage getLastGeneralLogs(MethodType methodTypeEnum, int count)
        {
            List<GeneralLog> logsList = new GeneralLogRepository().GetTopByMethodTypeEnum(methodTypeEnum, count);
            if (logsList != null && logsList.Count >= 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Logs = logsList.Select(y => new { y.LogTime, y.LogMessage }) });
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        #endregion

    }
}
